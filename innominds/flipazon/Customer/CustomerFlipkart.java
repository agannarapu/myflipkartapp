package com.innominds.flipazon.Customer;

import com.innominds.flipazon.Entity.dependencies.Cart;

// local dependencies

import com.innominds.flipazon.Entity.dependencies.Order;
import com.innominds.flipazon.Entity.Product;
import com.innominds.flipazon.Address;

/**
 * This is an implementation of customer class.
 * @author agannarapu
 */
public class CustomerFlipkart {
	
	private static int ID_COUNTER = 25324;
	
	private String 	id; 
	private String 	name;
	private String 	password;
	private String 	profileDescription;
	private Address customerAddress;
	
	private Cart customerCart;
	
	/**
	 * Constructor for CustomerFlipkart
	 * @param name					name of the customer to be created
	 * @param password				password of the customer to be created
	 * @param profileDescription	description of profile of customer
	 */
	public CustomerFlipkart(String name, String password, String profileDescription, String dno, String village, String city, long pincode, String state, String Country) {
		this.id 				= ++ID_COUNTER + "";
		this.name 				= name;
		this.password 			= password;
		this.profileDescription = profileDescription;
		System.out.println(this);
		this.customerAddress 	= new Address(dno, village, city, pincode, state, Country);
	    
		this.customerCart = new Cart();
//		System.out.println(this);
	}
	
	/**
	 * Overriding the default toString method of Customer so that customer can
	 * be printed in human readable format.
	 */
	@Override
	public String toString() {

		String stringToReturn = "";
		
		stringToReturn += "id:                " + this.id + "\n";
		stringToReturn += "name:              " + this.name + "\n";
		stringToReturn += "profileDescription " + this.profileDescription + "\n";
		
		return stringToReturn;
	}

	/**
	 * Helper method to add a product, provided as productToAdd, to the cart
	 * @param productToAdd Product object to be added to customer cart
	 */
	public void addProductToCart(Product productToAdd) {
		this.customerCart.addProduct(productToAdd);
	}
	
	/**
	 * Helper method to remove all products from the Customer cart.
	 */
	public void removeAllProductsFromCart() {
		this.customerCart.removeAllProducts();
	}

	/**
	 * Return the entire cart as a human readable string
	 * @return
	 */
	public String returnEntireCartAsString() {
		return this.customerCart.returnAllProductsAsString();
	}

	/**
	 * Helper method to place order for the entire cart
	 */
	public Order placeOrder() {
		
		// create an Order object, and return it
		Order myOrder = new Order(this.customerCart, this.customerAddress);
		this.customerCart.removeAllProducts();
		
		return myOrder;
	}

 
}

