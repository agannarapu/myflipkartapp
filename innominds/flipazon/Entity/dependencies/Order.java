package com.innominds.flipazon.Entity.dependencies;

import java.util.Iterator;

// local depenedencies
import com.innominds.flipazon.Address;
import com.innominds.flipazon.Entity.Product;

/**
 * This is an implementation of the Order placed by the customer.
 * @author agannarapu
 *
 */
public class Order {
	
	private String id="100";
	private Product[] products;
	private Address deliveryAddress;
	private String orderStatus;
	private double orderPrice;
	
	/**
	 * Constructor to create an order from a cart.
	 * @param orderCart
	 * @param addr
	 */
	public Order(Cart orderCart, Address addr) {
		this.id =id ;
		products = orderCart.getAllProductsInCart();
		this.orderPrice = 0;
		for (int i = 0; i < products.length; i++) {
			this.orderPrice += products[i].getPrice();
		}
		this.deliveryAddress = addr;
		this.orderStatus = "ORDER ACCEPTED";
	}
	
	/**
	 * Overriding the toString method to print the order in human readable form
	 */
	@Override
	public String toString() {

		String returnString = "";

		returnString += "id:             "  + this.id + "\n";
		returnString += "product count:  "  + this.products.length + "\n";
		returnString += "deliverAddress: \n" + this.deliveryAddress.toString() + "\n";
		returnString += "orderStatus:    "  + this.orderStatus + "\n";
		returnString += "orderPrice:     "  + this.orderPrice + "\n";
		
		return returnString;
	}

}
