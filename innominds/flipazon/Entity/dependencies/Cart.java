package com.innominds.flipazon.Entity.dependencies;

// Java dependencies
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// local dependencies
import com.innominds.flipazon.Entity.Product;

/**
 * This is an implementation of a Cart object. Each customer has a single cart.
 * @author agannarapu
 *
 */
public class Cart {
	
	private List<Product> cartProducts;
	
	/**
	 * Constructor for Cart
	 */
	public Cart() {
		cartProducts = new ArrayList<Product>();
	}
	
	/**
	 * Helper method which accepts a Product object and adds it to the cart
	 * @param productToAdd This is the Product object to add to the cart
	 */
	public void addProduct(Product productToAdd) {
		cartProducts.add(productToAdd);
	}

	/**
	 * Helper method to remove all the products from the cart
	 */
	public void removeAllProducts() {
		cartProducts.removeAll(cartProducts);
	}
	
	/**
	 * Helper method to return all the products in a human readable form
	 * @return
	 */
	public String returnAllProductsAsString() {
		String returnString = "";
		
		for (Product currProduct : cartProducts) {
			returnString += currProduct.toString() + "\n";
		}
		
		return returnString;
	}

	/**
	 * Is a helper method to convert the ArrayList<Product> of cart itmes
	 * into Product[] and return it.
	 * @return a Product[] of all items in the cart
	 */
	public Product[] getAllProductsInCart() {
		int sizeOfItems = cartProducts.size();
		Product[] returnArray = new Product[sizeOfItems];
		
		int index = 0;
		for (Iterator<Product> iterator = cartProducts.iterator(); iterator.hasNext();) {
			Product currProduct = iterator.next();
			returnArray[index] = currProduct;
			
			index++;
		}
		
		return returnArray;
	}
}
