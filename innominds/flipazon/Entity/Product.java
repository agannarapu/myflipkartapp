package com.innominds.flipazon.Entity;


/**
 * This is the Product class for implementation of  the product details
 * @author agannarapu
 */
public class Product {
	
	private static int	ID_COUNTER = 300;
	
	private String 	name;
	private String 	description;
	private double 	price;

	private String 	id;
 
	/**
	 * Constructor for creating a new Product with unique ID
	 * @param name 			name of the product
	 * @param description	description of the product
	 * @param price			price of the product
	 */
	public Product(String name, String description, double price) {
		this.id = 			++ID_COUNTER + "";
		this.name = 		name;
		this.description = 	description;
		this.price = 		price;
		System.out.println(this);
	}
	
	/**
	 * Helper method to return the product details entered by user as a
	 * human readable string string
	 * @return A string which represents the product
	 */
	
	public String toString() {
		String returnString = "Product Details are:\n";
		
		returnString += " id:	       " + this.id + "\n";
		returnString += " name:        " + this.name + "\n";
		returnString += " description: " + this.description+ "\n";
		returnString += " price:       " + this.price + "\n";
		returnString +="=================================================";
		
		return returnString;
	}
	
	/**
	 * This is the getter method for getting the price of this product
	 * @return
	 */
	public double getPrice() {
		return this.price;
	} 
}