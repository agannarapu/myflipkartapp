package com.innominds.flipazon;

// local dependencies
import com.innominds.flipazon.Customer.CustomerFlipkart;
import com.innominds.flipazon.Entity.Product;
import com.innominds.flipazon.Entity.dependencies.Cart;
import com.innominds.flipazon.Entity.dependencies.Order;

/**
 * This is a Tester class to test the Flipkart Application
 * @author agannarapu
 *
 */
public class Tester { 
	
	/** Main method */
	public static void main(String[] args) {
		
		Tester myTester = new Tester();
		// myTester.testCart();
		
		myTester.testCustomer();
		
		
	
	}
	
	/**
	 * Helper method to test the Customer creation implementation
	 */
	private void testCustomer() {
		// create a customer object
		CustomerFlipkart cf = new CustomerFlipkart("Ashok", "password123", "Ashok's profile description", "201", "Sardar Patel", "Hyd", 530065l, "telangana", "india");
		
		// creating new products
		Product prod1, prod2, prod3;
		prod1 = new Product("Shoes", "This is shoe description", 2);
		prod2 = new Product("Socks", "Socks description", 344);
		prod3 = new Product("TV", "TV Description", 23535);
		
		// adding products
		cf.addProductToCart(prod1);
		cf.addProductToCart(prod2);
		cf.addProductToCart(prod3);
		
		// print the cart
		System.out.println(cf.returnEntireCartAsString());

		// place the order
		Order orderPlaced = cf.placeOrder();
		// print the order
		System.out.println(orderPlaced);
		
		// print the cart again
		System.out.println(cf.returnEntireCartAsString());
	}

	/**
	 * Helper method to test the Cart object implementation
	 */
	public void testCart() {
		
		Product prod1, prod2, prod3;
		prod1 = new Product("Shoes", "This is shoe description", 2);
		prod2 = new Product("Socks", "Socks description", 344);
		prod3 = new Product("TV", "TV Description", 23535);
		
		// create a cart
		Cart myCart = new Cart();
		
		// add something to the cart
		myCart.addProduct(prod1);
		myCart.addProduct(prod2);
		myCart.addProduct(prod1);
		
		myCart.removeAllProducts();
		
		System.out.println(myCart.returnAllProductsAsString());
		
	}
}

	    	 