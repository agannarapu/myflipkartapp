package com.innominds.flipazon;
/**
 * 
 * @author agannarapu
 *This is the Address class to implement the customer address details
 */
public class Address {
	String dno;
	String Village;
	String city;
	long pincode;
	String state;
	String Country;
	public Address(String dno,String Village,String city,long pincode,String state,String Country) {
		  this.dno=dno;
		  this.Village=Village;
		  this.city=city;
		  this.pincode=pincode;
		  this.state=state;
		  this.Country=Country;
		  System.out.println(this);
 		  
	}
	public String toString() {
		String ReturnValues = "This is a customer Address details:\n";
		
		ReturnValues += " dno     :	    "   +        this.dno      + 			"\n";
		ReturnValues += " Village :	    "   +        this.Village  + 			"\n";
		ReturnValues += " City    :	    "   +        this.city     +	        "\n";
		ReturnValues += " Pincode :     "   +        this.pincode  +            "\n";
		ReturnValues += " State   :     "   +        this.state    + 			"\n";
		ReturnValues += " Country :     "   +        this.Country  +       "\n";
		
		return ReturnValues;
	}
	

}
